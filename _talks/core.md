---
title: "Core Team Keynote"
kind: talk
start: 2017-10-27T09:30:00
end: 2017-10-27T10:00:00
speaker: Aaron Turon, Carol Nichols, Niko Matsakis
twitter:
website:
headshot: core-team.jpg
bio:
---

Aaron, Carol, and Niko will deliver an update on the state of all things Rust.
You don't want to miss it!
