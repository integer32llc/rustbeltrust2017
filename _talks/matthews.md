---
title: "The Story of Stylo: Replacing Firefox's CSS engine with Rust"
kind: talk
start: 2017-10-27T11:30:00
end: 2017-10-27T12:00:00
speaker: Josh Matthews
twitter: lastontheboat
website: https://joshmatthews.net
headshot: matthews.jpg
bio: >
  Josh Matthews builds web browsers and sustainable communities for a living at Mozilla.
  He gets excited about demolishing barriers to participating in open source projects.
  He also sings in a barbershop quartet.
---

Firefox is in the process of shipping a new implementation of CSS
styling written in Rust as part of Mozilla's Servo project.
Firefox has 20+ million lines of code and hundreds of millions of users,
so this is no small undertaking!
As a case study for integrating a large,
multi-repo Rust project into a larger C++ project,
this talk explains how we:

* created bi-directional FFI boundaries that maximized each project's strengths
* addressed cross-language mismatches including threadsafety and ownership
* integrated two fast-moving codebases while managing regressions

We'll cover specific successes and failures that emerged over the course of the project,
and discuss how Rust contributed to those outcomes.
