---
title: "Refining Your Project"
kind: workshop
length: half day
room: Spartan
start: 2017-10-26T09:00:00
end: 2017-10-26T12:00:00
speaker: Andrew Hobden
twitter: andrewhobden
website: https://hoverbear.org
headshot: hobden.jpg
bio: >
  Growing up in the great forests of Canada,
  this bear hovered over to Berlin to work with other Rustaceans.
---

You did it!
You made a cool thing!
It works!
(Ok, sure, maybe only in your small test cases.)

What now?

Now you need to refine it!
That means writing documentation,
refactoring,
building tests,
benchmarking,
making releases,
dealing with issues,
landing PRs,
oh my!

In this workshop we'll discuss and get our hands dirty with the projects of our participants.

First, we'll all have a chance to share insights we've earned from our experiences,
then we'll pair (or triple) up with other attendees and try to use each others projects,
documenting the stumbling blocks and good techniques as we go.

At the end of it all you'll have a list of things you can work on in your project,
a list of ideas from others that work well,
and hopefully at least one new Rusty friend (and user of your thing!)
