---
title: 'Building on an unsafe foundation: What "unsafe" means in Rust and how to deal with that'
kind: talk
start: 2017-10-27T15:00:00
end: 2017-10-27T15:30:00
speaker: Jason Orendorff
twitter: jorendorff
website: http://jorendorff.blogspot.com/
headshot: orendorff.jpg
bio: >
  Jason Orendorff lives just outside Nashville, Tennessee.
  He's a hacker for Mozilla and the coauthor of <i>Programming Rust</i>,
  the O'Reilly book with the crab on the cover.
---

Beneath every safe programming language, library,
or virtual machine is a whole lot of unsafe code.
Rust is no exception.
This talk will explain why "unsafe" code is allowed at all in a safe language,
show the unsafe code at work inside safe features like `Vec`,
and teach how to write a safe library that uses unsafe code.
