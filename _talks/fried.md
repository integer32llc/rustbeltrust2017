---
title: "The Long Road to Creative Programming"
kind: talk
start: 2017-10-27T12:00:00
end: 2017-10-27T12:30:00
speaker: Phil Fried
headshot: fried.jpg
bio: >
  Phil has been a software developer for a while now,
  but before that he was a painter and sculptor.
  These days,
  he spends his time working for a big data startup in Columbus,
  and creating tooling for event-based microservices.
  He loves hiking, camping, climbing,
  and talking smack about art, programming, and the convergence of the two.
---

Creativity as a concept is not generally well understood,
and that's especially true as it applies to programming.
Creativity can be either invaluable or dangerous,
and sometimes it's both.
By understanding creativity,
you'll be able to leverage it to build awesome software.
In this talk,
we'll explore what it means to be creative and how it relates to programming,
and especially to Rust.
Expect to come away with some tips for how to let your creativity flourish.
