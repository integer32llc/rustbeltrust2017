---
title: "Unconference"
kind: workshop
length: all day
room: Athenian
start: 2017-10-26T09:00:00
end: 2017-10-26T17:00:00
speaker: You
twitter:
website:
headshot: unconference.png
bio: >
  You were chosen in 2006 as Time magazine's Person of the Year.
  This award recognized the millions of people who anonymously contribute
  user-generated content to wikis such as Wikipedia, YouTube, MySpace, Facebook,
  and the multitudes of other websites featuring user contribution.
  (<a href="https://en.wikipedia.org/wiki/You_(Time_Person_of_the_Year)">From Wikipedia</a>)

---

For this workshop track,
*you* will decide the schedule!
Bring topics you'd like to chat with folks about,
code to work on,
or questions.
At the start of the day,
Jean Lange will facilitate the collaborative creation of the schedule for the rest of the day.
This is a great chance to talk to folks in person that you normally converse with online!
