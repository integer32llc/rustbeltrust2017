---
title: "Intro to Rust"
kind: workshop
length: half day
room: Macedonian
start: 2017-10-26T09:00:00
end: 2017-10-26T12:00:00
speaker: Ben Striegel
twitter: bstrie
headshot: striegel.jpg
bio: >
  Ben Striegel is one of the earliest members of the Rust programming language community,
  having been an active member of the project since 2011.
  Today, Ben is a member of Rust’s official community outreach team and focuses on
  teaching and mentoring newcomers and interfacing with other open source communities.
---

Systems programming has long been sold as a school of hard knocks,
where competency could only be attained through a decade's worth of
experience staring at segmentation faults,
scowling at core dumps,
and debugging inscrutable runtime and concurrency errors.

Enter Rust: a new systems programming language from Mozilla that
matches the speed and memory overhead of C++ while providing
best-in-class compile-time protection against memory errors and
concurrency bugs.

Currently being used to reimplement pieces of Firefox,
Rust bridges the gap between high-level and low-level programmers by
focusing on usability and offering a suite of tools that cater to
modern development practices.
This hands-on workshop is for programmers of any background,
from Java to Python to C and beyond,
and will provide an introduction to the Rust ecosystem and get you up
and running with the basics of the language.
