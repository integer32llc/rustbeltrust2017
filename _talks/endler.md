---
title: "Live coding like no one's watching!"
kind: workshop
length: half day
room: Macedonian
start: 2017-10-26T14:00:00
end: 2017-10-26T17:00:00
speaker: Matthias Endler
twitter: matthiasendler
website: https://matthias-endler.de/
headshot: endler.jpg
bio: >
  Matthias is a Backend Engineer located in Düsseldorf, Germany.
  His interests are scalability, performance and distributed systems.
  At work he improves the infrastructure at trivago by making it faster and more reliable.
  When not on his laptop he plays guitar and drinks hot chocolate.
---

So, you've heard of this "Rust" last year.
Still it feels a bit like self-assembly furniture:
you don't know how all the parts fit together.
Watch me make a fool of myself by trying to solve some common programming tasks in Rust.

Highlights: Nervous laughter, self-demeaning humor, browsing the Rust documentation for clues.
I might explain a little Rust along the way.

This is gonna be a hands-on,
interactive session,
so beginners and pros are equally welcome to join.
