---
title: "Becoming a Contributor"
kind: talk
start: 2017-10-27T16:00:00
end: 2017-10-27T16:30:00
speaker: Chris Krycho
twitter: chriskrycho
website: http://www.chriskrycho.com
headshot: krycho.jpg
bio: >
  Chris Krycho is a senior software engineer at <a href="https://www.olo.com">Olo</a>,
  where he spends most of his time writing TypeScript in Ember.js.
  He has hosted the <a href="http://www.newrustacean.com">New Rustacean</a> podcast
  for the last couple years,
  trying to build up the Rust community both by providing teaching resources and by
  helping create enthusiasm and a way for people to learn about the programming language
  without ever having written a line of code.
---

So, you're new to the Rust community.
(Or any community, really!)
And you want to help, but, well, you're *new*.
So how exactly do you start contributing?
What kinds of contributions are valuable?
We'll talk about everything from asking questions to writing documentation,
from pitching in on forums and chat to writing blog posts,
and from starting your own projects to contributing to other open-source projects.
