---
title: "Traits and You: A Deep Dive"
kind: talk
start: 2017-10-27T17:00:00
end: 2017-10-27T17:30:00
speaker: Nell Shamrell-Harrington
twitter:
website:
headshot: shamrell-harrington.jpg
bio: >
  Nell Shamrell-Harrington is a Software Development Engineer at Chef
  and a core maintainer of the Habitat open source project.
  She also sits on the advisory board for the University of Washington
  Certificates in Ruby Programming and DevOps.
  She specializes in Open Source, Chef, Rust, Ruby, Rails, Regular Expressions,
  and Test Driven Development and has traveled the world speaking on these topics.
  Prior to entering the world of software development,
  she studied and worked in the field of theatre.
---

Traits are one of the most powerful,
but also most difficult parts of Rust to master.
Come to this talk for a visual exploration of how traits work -
from the most basic to the advanced.
It is only through deep understanding of a concept like traits that
you can fully harness their power in your every day code.
You will walk away with a deep understanding of how traits work,
why they work the way they do, and the how and why of using them.
