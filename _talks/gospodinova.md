---
title: "Rust support for KDevelop"
kind: talk
start: 2017-10-27T10:30:00
end: 2017-10-27T11:00:00
speaker: Emma Gospodinova
twitter:
website: https://perplexinglyemma.blogspot.com/
headshot: gospodinova.jpg
bio: >
  Emma is a third year undergraduate at Imperial College London.
  She worked at Microsoft Research in summer 2016 on the AssessMS project.
  She started working on a Rust language support plugin as part of Google Summer of Code 2017.
  She has been programming for around 10 years.
  She is interested in several different areas of computer science,
  namely artificial intelligence, computer vision, computer graphics,
  and most recently, compilers, type systems and operating systems and
  has worked on various different projects in these fields during high school and university.
  In her free time, she’s currently working on a hobby OS in Rust.
---

Rust was voted “most loved” language by developers for the second year
in a row in the Stack Overflow developer survey.
There have been projects made using Rust on everything from operating
systems to game engines for Minecraft-like games.
Despite this, IDE support is still very limited.

As my Google Summer of Code project,
I worked on a Rust plug-in for the KDevelop IDE which aimed to support most standard IDE features such as
semantic highlighting, code completion, project management and debugging.
I will go through the challenges of integrating language support in an existing IDE and
talk about how adding semantic highlighting was one line of code and
getting debugging to work took less than 10 minutes.
