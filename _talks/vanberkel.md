---
title: "Find planets with Rust"
kind: workshop
length: half day
room: Spartan
start: 2017-10-26T14:00:00
end: 2017-10-26T17:00:00
speaker: Daan van Berkel
twitter: daan_van_berkel
website: http://dvberkel.nl/
headshot: vanberkel.jpg
bio: >
  Daan van Berkel is a enthusiastic software craftsman with a knack for
  presenting technical details in a clear and concise manner.
  Driven by the desire for understanding complex matters,
  Daan is always on the lookout for innovative uses of software
---

How are exo-planets found?
With Rust of course!

Exo-planets, planets that orbits distant stars,
are a scientific marvel that appeal to our adventurous mind.
In artist's impressions they are portrayed as beautiful alien worlds
dancing around their host star.
The real world is not so visual but far more interesting.

During this workshop, you will learn the tricks and develop the tools
with which you can analyse real-*outer*-world data in search of
exo-planets.
You will get your amateur astronomer merit badge when you find the hidden worlds.
