---
title: "RustBridge"
kind: workshop
length: all day
room: Corinthian
start: 2017-10-26T09:00:00
end: 2017-10-26T17:00:00
speaker: Ashley Williams
twitter: ag_dubs
website: https://github.com/ashleygwilliams
headshot: williams.jpg
bio: >
  Ashley is a Services Engineer at npm, Inc.
  She also represents the Individual Membership on the Node.js Foundation Board of Directors.
  In her spare time,
  she contributes to the Rust Programming Language and its package manager, Cargo.
  She is also a founding organizer of the NodeTogether and RustBridge
  educational initiatives for underrepresented persons in tech.
---

RustBridge is a workshop focused on getting underrepresented people with a background in another programming language to learn Rust and join the community.

Format-wise: RustBridge is an all day event (~8hours). The first portion focuses on Rust language syntax and semantics, relating them to programming concepts in other languages. It consists of a presentation followed by guided and self-directed practice using exercism.io. The second portion leads students in a hands-on project: building a small website in Rust.

<img alt="RustBridge logo" src="/assets/rustbridge.svg" />
